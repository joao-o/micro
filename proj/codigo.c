/* commands available:
 *   accel <target speed (rpm)> <initial wait time(timer ticks)> <initial speed (rpm)>
 *     accelerate motor to target speed while waiting "inital wait time" at "initial speed"
 *   adcrb <motor int> <delay>
 *     start 700 samples burst read with the adc at motor interrupt "motor int" with delay "delay"
 *   stop
 *     stop the motor
 *   getspd
 *     show motor speed in RPM
 *   adcpb 
 *     print the values stored in "data" to the serial console
 */

#include <libpic30.h>		//C30 compiler definitions
#include <uart.h>		//UART (serial port) function and utilities library
#include <timer.h>		//timer library
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <adc10.h>


#define FCY ((long) 7372*4)	//instruction frequency in kHz

//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16);	//oscilator at 16x PLL
_FWDT(WDT_OFF);			//watchdog timer is off

#define RXBSIZE 80
#define M_CONST 2000
#define F1 (unsigned int) 60625	//constants used to compute the timer value
#define F2 (unsigned int) 57

int str_pos = 0;
char RXbuffer[RXBSIZE];		//buffer used to store characters from serial port
char interpbuf[RXBSIZE];

#define NCOMS  5		//number of commands
const char *comm[NCOMS] = { "accel", "getspd", "stop", "adcrb", "adcpb" };	//available commands
const int commsz[NCOMS] = { 5, 6, 4, 5, 5 };	//size of each command in bytes

int command_avail = 0;		//flag to indicate command availability
int ticks;			//global variable to provide timing from the timer isr
int data[700];
int adci;
int state;

//function to clear rxbuffer
void clean_RxB(void)
{
   while (str_pos) {
      str_pos--;
      RXbuffer[str_pos] = 0;
   }
}

//implements main acceleration routine
void ramp(int t)
{
   ticks = 0;
   printf("\rramp %d,%d\n", t, PR2);
   while (PR2 > t) {
      if (ticks >= 5) {		//change speed every 5 timer interrupts
	 PR2 -= (PR2 / M_CONST) * (PR2 / M_CONST) * (PR2 / M_CONST);
	 ticks = 0;
      }
   }
   PR2 = t;			// set the final speed
}

//wrapper routine for motor acceleration
void accel(int spd, int kick, int init)
{
   spd = ((F1 / spd) * F2) + ((F1 % spd) * F2) / F1;	//convert RPM to values of PR2
   init = ((F1 / spd) * F2) + ((F1 % init) * F2) / F1;
   if (kick != 0) {
      PR2 = init;
      _T2IE = 1;
      ticks = 0;
      while (ticks < kick );	//wait for the motor to start spinning before accelerating
   }
   ramp(spd);
   puts("\rdone");

}

//routine to start measurements with the adc 
void readadc(int dly, int sync)
{
   adci = 0;			//reset the index of data 
   while (state != sync);	//wait for the motor to be in the selected position
   for (; dly; dly--);		//wait for dly cycles as to start sampling the the optimal position
   ADCON1bits.ASAM = 1;		//start sampling
}

//interpret command
void interp_command(void)
{
   int a1, a2, a3, i = 0;
   command_avail = 0;
   memcpy(interpbuf, RXbuffer, str_pos + 1);
   //copy string to temporary buffer to free up RXbuffer
   clean_RxB();
   interpbuf[str_pos - 1] = 0;
   //put a '0' at end of the string to allow usage of stdlib functions
   printf("\r%s\n", interpbuf);
   for (i = 0; i < NCOMS; i++) {	//compare read string with available commands
      if (!strncmp(comm[i], interpbuf, commsz[i]))
	 break;
   }
   switch (i) {			//execute the command
   case 0:
      sscanf(interpbuf + commsz[1], "%d %d %d", &a1, &a2, &a3);
      accel(a1, a2, a3);
      break;
   case 1:
      printf("\rspd = %d\n", (F1 / PR2) * F2 + ((F1 % PR2) * F2) / F1);
      break;
   case 2:			// disable timer interrupt and switch off motor
      _T2IE = 0;
      _LATE3 = 0;
      _LATE1 = 0;
      break;
   case 3:
      sscanf(interpbuf + commsz[1], "%d %d", &a1, &a2);
      readadc(a1, a2);
      break;
   case 4:			//dump measurement data to serial terminal
      for (i = 0; i < 700; i++)
	 printf("\r%d\n", data[i]);
      adci = 0;
      break;
   default:
      puts("\rcommand not found\n");
   }

}

void __attribute__ ((interrupt, auto_psv)) _U2RXInterrupt(void)
{
   static int online = 0;

   IFS1bits.U2RXIF = 0;
   while (U2STAbits.URXDA) {
      RXbuffer[str_pos] = U2RXREG;
      str_pos++;		//increments the position in the buffer to store the next char
      if (str_pos >= RXBSIZE) {
	 str_pos = 0;
      }
   }
   if (RXbuffer[str_pos - 1] == '\r') {
      if (!online) {
	 puts("\rOK");
	 online = 1;
	 clean_RxB();
      } else {
	 command_avail = 1;
      }
   }
}

// adc interrupt routine, copy adc result into 'data'
void __attribute__ ((interrupt, auto_psv)) _ADCInterrupt(void)
{
   _ADIF = 0;
   data[adci++] = ADCBUF0;
   if (adci == 700)
      ADCON1bits.ASAM = 0;
}

//timer2 ISR
//the motor eletrical cycle uses a period of 32 interruptions
//however sync with the mechanical cycle is needed so internally
//a cycle of 64 states is used this way we can tell in which half 
//of the mechanical cycle the motor is.
//(an eletrical cycle is half a mechanical one)

void __attribute__ ((interrupt, auto_psv)) _T2Interrupt(void)
{
   _T2IF = 0;

   switch (state % 32) {
   case 0:
      _LATE3 = 1;
      break;
   case 10:
      _LATE3 = 0;
      break;
   case 16:
      _LATE1 = 1;
      break;
   case 26:
      _LATE1 = 0;
      break;
   }
   asm("btg LATD, #2");
   state = (state > 63) ? 0 : state + 1;
   ticks++;
}

int main(void)
{
   unsigned int UMODEvalue, U2STAvalue, TCONval;

   //configure motor pins
   //E0:EN1 E1:IN1 E2:EN2 E3:IN2
   _LATE2 = 1;
   _LATE0 = 1;
   _LATE1 = 0;
   _LATE3 = 0;
   _TRISE0 = 0;
   _TRISE1 = 0;
   _TRISE2 = 0;
   _TRISE3 = 0;
   _TRISD2 = 0;
   _TRISD3 = 0;

   // Serial port config
   //activates the uart in continuous mode (no sleep) and 8bit no parity mode
   UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
   //activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
   U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
   OpenUART2(UMODEvalue, U2STAvalue, 15);	//configures and activates UART2 at 115000 bps
   U2STAbits.URXISEL = 1;
   _U2RXIE = 1;			//0-Interruption off, 1-Interruption on
   U2MODEbits.LPBACK = 0;	//disables hardware loopback on UART2. Enable only for tests
   U2MODEbits.STSEL = 0;

   // Configures Timer2
   TCONval = T2_ON & T2_GATE_OFF & T2_PS_1_8 & T2_SOURCE_INT;
   OpenTimer2(TCONval, 30000);

   __C30_UART = 2;		//define UART2 as predefined for use with stdio library, printf etc
   // ADC configuration
   SetChanADC10(ADC_CH0_POS_SAMPLEA_AN0 & ADC_CH0_NEG_SAMPLEA_AN1);
   OpenADC10(ADC_MODULE_ON & ADC_IDLE_STOP & ADC_FORMAT_INTG & ADC_CLK_AUTO &
	     ADC_AUTO_SAMPLING_ON & ADC_SAMP_OFF,
	     ADC_VREF_AVDD_AVSS & ADC_CONVERT_CH0 & ADC_SAMPLES_PER_INT_1 &
	     ADC_SCAN_OFF & ADC_ALT_BUF_OFF & ADC_ALT_INPUT_OFF,
	     ADC_SAMPLE_TIME_1 & ADC_CONV_CLK_5Tcy & ADC_CONV_CLK_SYSTEM,
	     ENABLE_ALL_DIG, SCAN_NONE);
   _ADIE = 1;
   ADPCFG = 0x0004;
   ADCON1bits.ASAM = 0;


   puts("\n\rSerial port ONLINE");	//to check if the serial port is working

   // Begin main execution cycle
   while (1) {
      if (command_avail)
	 interp_command();
   }

}
