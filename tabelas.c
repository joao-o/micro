/* programa potencialmente util para gerar tabelas em latex
 * invocação:
 * tabelas <ficheiro de dados> <string de formato>
 * o programa lê o ficheiro de dados linha a linha e escreve cada linha lida num novo ficheiro
 * de acordo com a string de formato
 * o ficheiro de dados deve estar formatado em três colunas separadas por espaços
 * a string de formato é uma do estilo do printf em que existem três argumentos do tipo float
 * cada um correspondendo a uma coluna
 * ex: para por todos os valores separados por um '&' a string de formato deverá ser:
 * '%f&%f&%f'
 * o resultado é escrito num ficheiro com o nome do original mas com a extensão
 * definida no cabeçalho em baixo
 */  
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define EXT ".txtbl"

int i;

char * extsub (char *s, char *ext)
{
  char* r;

  for(i=0; s[i]!=0 && s[i]!='.' ;i++);

  r=malloc(i+1+strlen(ext));

  strcpy(r,s);
  strcpy(r+i,ext);

  return r;
}

int main (int argc, char **argv)
{
  FILE *fpr, *fpw;
  char *n,buf[200];
  double c1,c2,c3;

  if (argc != 3) {
      puts ("?");
      exit (-1);
  }

  fpr = fopen (argv[1], "r");
  if (fpr == NULL) {
      puts ("?");
      exit (-1);
  }

  n=extsub(argv[1],EXT);
  fpw = fopen (n,"w");
  free(n);

  while( fgets(buf,200,fpr)!= NULL ){
    sscanf(buf,"%lf %lf %lf",&c1,&c2,&c3);
    fprintf(fpw,argv[2],c1,c2,c3);
    fputc('\n',fpw);
  }
}
