 
Disassembly Listing for t0
Generated From:
/home/user/micro_cj/t0.X/dist/default/production/t0.X.production.elf
Sep 16, 2014 11:54:30 AM

---  /home/user/micro_cj/t0.X/main_lb0.c  ---------------------------------------------------------------
1:                 /*
2:                  * File:   tutorial.c
3:                  * Author: joao
4:                  *
5:                  * Created on 24 de Setembro de 2012, 23:16
6:                  */
7:                 
8:                 #include <p30Fxxxx.h>      /* Device header file */
9:                 #include <stdio.h>
10:                #include <stdlib.h>
11:                
12:                /* Microcontroller MIPs (FCY) */
13:                #define SYS_FREQ        16*7372800L
14:                #define FCY             SYS_FREQ/4
15:                
16:                //Configuration bits
17:                _FOSC(CSW_FSCM_OFF & XT_PLL16);  //Run this project using an external crystal
18:                                                //routed via the PLL in 16x multiplier mode
19:                                                //For the 7.3728 MHz crystal we will derive a
20:                                                //throughput of 7.3728e+6*16/4 = 29.4 MIPS(Fcy)
21:                                                //,~33.9 nanoseconds instruction cycle time(Tcy).
22:                _FWDT(WDT_OFF);                 //Turn off the Watch-Dog Timer.
23:                _FBORPOR(MCLR_EN & PWRT_OFF);   //Enable MCLR reset pin and turn off the
24:                                                //power-up timers.
25:                _FGS(CODE_PROT_OFF);            //Disable Code Protection
26:                
27:                
28:                int main(void) {
000194  FA0000     LNK #0x0
30:                    _TRISE8 = 1;
000196  A802D9     BSET 0x2D9, #0
31:                    _TRISF0 = 0;
000198  A902DE     BCLR TRISF, #0
00019A  370001     BRA 0x19E
00019C  000000     NOP
33:                    while(1){
34:                        if(_RE8 == 0)
00019E  8016D1     MOV PORTE, W1                //(1)
0001A0  201000     MOV #0x100, W0               //(1)
0001A2  608000     AND W1, W0, W0               //(1)
0001A4  500FE0     SUB W0, #0x0, [W15]          //(1)
0001A6  3AFFFA     BRA NZ, 0x19C                //(1)
35:                        {
36:                            if(_RF0 == 0){
0001A8  801700     MOV PORTF, W0                //(1)
0001AA  600061     AND W0, #0x1, W0             //(1)
0001AC  500FE0     SUB W0, #0x0, [W15]          //(1)
0001AE  3A0002     BRA NZ, 0x1B4                //(3/2)
37:                                _LATF0 = 1;
0001B0  A802E2     BSET LATF, #0                //(1)
0001B2  37FFF5     BRA 0x19E                    //(2)
38:                            } else {
0001B4  A902E2     BCLR LATF, #0                //(1)
0001B6  37FFF3     BRA 0x19E                    //(2)
