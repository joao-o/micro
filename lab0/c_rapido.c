#include <p30Fxxxx.h>      //Device header file
#include <stdio.h>
#include <stdlib.h>

/* Microcontroller MIPs (FCY) */
#define SYS_FREQ        16*7372800L
#define FCY             SYS_FREQ/4

//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16);  
/* Run this project using an external crystal
   routed via the PLL in 16x multiplier mode
   For the 7.3728 MHz crystal we will derive a
   throughput of 7.3728e+6*16/4 = 29.4 MIPS(Fcy)
   ,~33.9 nanoseconds instruction cycle time(Tcy). */

_FWDT(WDT_OFF);                 //Turn off the Watch-Dog Timer.
_FBORPOR(MCLR_EN & PWRT_OFF);   //Enable MCLR reset pin and turn off the
                                //power-up timers.
_FGS(CODE_PROT_OFF);            //Disable Code Protection


int main(void) {
    
    /* set pin of led 6 as output and pin of sw4 as input */
    _TRISE8 = 1;
    _TRISF0 = 0;

    while(1){ // main loop
        if(_RE8 == 0)        // test state of SW4
        {
            if(_RF0 == 0){   // test state of led 6 and toggle it
                _LATF0 = 1;
            } else {
                _LATF0 = 0;
            }
        }
    }
    return (EXIT_SUCCESS);
}
