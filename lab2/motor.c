/*
 * File:   newmain.c
 * Author: user
 *
 * Created on September 30, 2014, 9:33 AM
 */
#include <libpic30.h>		        //C30 compiler definitions
#include <uart.h>			//UART (serial port) function and utilities library
#include <timer.h>			//timer library
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


#define FCY ((long) 7372*4) 		//instruction frequency in kHz
#define LED2 LATDbits.LATD3
#define LED1 LATDbits.LATD2

//Configuration bits
 _FOSC(CSW_FSCM_OFF & XT_PLL16);  	//oscilator at 16x PLL
 _FWDT(WDT_OFF);  					//watchdog timer is off

#define RXBSIZE 80
char RXbuffer[RXBSIZE];	//buffer used to store characters from serial port
char intrepbuf[RXBSIZE];
int str_pos = 0; 	//position in the RXbuffer
int online = 0;
int command_avail = 0;
int i;
#define NCOMM 2
const char *comm[NCOMM]={"spd","rot"};

void clean_RxB(void)
{
    while(str_pos){
        str_pos--;
        RXbuffer[str_pos] = 0;
    }
}

void speed_mod(void)
{
    printf("\rspeed: %d\n",atoi(intrepbuf+3));
}

void rotation_mod(void)
{
    printf("\rrot: %c\n",intrepbuf[4]);
}

void intrep_command(void)
{
    command_avail=0;
    memcpy(intrepbuf,RXbuffer,str_pos+1);
    intrepbuf[str_pos-1] = 0;
    clean_RxB();
    printf("\r%s\n",intrepbuf);
    for (i=0;i<NCOMM;i++)
        if(!strncmp(intrepbuf,comm[i],3))
            break;
    switch (i){
            case 0:
                speed_mod();break;
            case 1:
                rotation_mod();break;
    }
}

void __attribute__((interrupt,auto_psv)) _U2RXInterrupt(void)
{

    IFS1bits.U2RXIF = 0;
    while(U2STAbits.URXDA)
    {
        RXbuffer[str_pos] = U2RXREG;
        str_pos++;						//increments the position in the buffer to store the next
        if(str_pos >= RXBSIZE){str_pos = 0;}
    }
    if (RXbuffer[str_pos-1] == '\r'){
        if (!online){
            printf("\rOK\n");
            online = 1;
            clean_RxB();
        }
        command_avail = 1;
    }

}

void delay_ms(unsigned int delay) //delay in miliseconds
{
    unsigned int cycles;	// number of cycles
    for(;delay;delay--)
        for(cycles=FCY/4;cycles;cycles--); //~1ms cycle
}

/* main code */
int main(void)
{
        int j;
	unsigned int UMODEvalue, U2STAvalue;

	//Configure pin 2 and 3 of port D as outputs -> LEDS
        _TRISE0 = 0;
        _TRISE1 = 0;
        _TRISE2 = 0;
        _TRISE3 = 0;
        _LATE2 = 1;
        _LATE0 = 1;

	/* Serial port config */
	UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT; //activates the uart in continuos mode (no sleep) and 8bit no parity mode
	U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX; //activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
 	OpenUART2 (UMODEvalue, U2STAvalue, 15); //configures and activates UART2 at 115000 bps
	U2STAbits.URXISEL = 1;
	_U2RXIE = 1; 			//0-Interruption off, 1-Interruption on
	U2MODEbits.LPBACK = 0; 		//disables hardware loopback on UART2. Enable only for tests
        U2MODEbits.STSEL = 0;

	__C30_UART = 2; 		//define UART2 as predefined for use with stdio library, printf etc


	printf("\n\rSerial port ONLINE \n"); 	//to check if the serial port is working

	/* Begin main execution cycle */
	while(1)
	{
            //if(command_avail)
              //  intrep_command();
            for(i=54;i>13;i--){
                printf("\r%d\n",i);
                for (j=10;j;j--) {
                  _LATE3 = 1;
                  delay_ms(i);
                  _LATE3 = 0;
                  delay_ms(i);
                  _LATE1 = 1;
                  delay_ms(i);
                  _LATE1 = 0;
                  delay_ms(i);
                }
            }
            
	}//end while(1)

}//end main
