/*
 * File:   newmain.c
 * Author: user
 *
 * Created on September 30, 2014, 9:33 AM
 */
#include <libpic30.h>           //C30 compiler definitions
#include <uart.h>               //UART (serial port) function and utilities library
#include <timer.h>              //timer library
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


#define FCY ((long) 7372*4)     //instruction frequency in kHz
#define LED2 LATDbits.LATD3
#define LED1 LATDbits.LATD2

//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16); //oscilator at 16x PLL
_FWDT(WDT_OFF);                 //watchdog timer is off

#define STEEP 1                 //steepness of acceleration curve
#define RXBSIZE 80
#define F1 (unsigned int) 11875
#define F2 (unsigned int) 291
char RXbuffer[RXBSIZE];         //buffer used to store characters from serial port
char intrepbuf[RXBSIZE];
int str_pos = 0;                //position in the RXbuffer
int online = 0;
int command_avail = 0;
int i, state;
unsigned int tmr2;

void clean_RxB(void)
{
   while (str_pos) {
      str_pos--;
      RXbuffer[str_pos] = 0;
   }
}

void ramp(int t)
{
   int u = (tmr2 < t) ? 1 : 0;
   _T3IE = 1;
   while ((tmr2 < t && u) || (tmr2 > t && !u)) {
       {
         if( i == 1){
           if (u) {
             tmr2 += ((tmr2 /253)*5  - 91);
           } else {
             tmr2 -= ((tmr2 /253)*5  - 91);
           }
           i=0;
           printf("\r%d\n",tmr2);
         }
      }
   }
   //PR2 = t;
   //tmr2 = t;
   _T3IE= 0;
}

void spd()
{
   int freq;
   freq = atoi(intrepbuf + 3);
   if (freq > 730){
       printf("\rerr: too fast!\n");
       return;
   }
   freq = F1/freq * F2;
   printf("\r%d\n", freq);
   ramp(freq);
   printf("\rdone\n");
}


void intrep_command(void)
{
   command_avail = 0;
   memcpy(intrepbuf, RXbuffer, str_pos + 1);
   intrepbuf[str_pos - 1] = 0;
   clean_RxB();
   printf("\r%s\n", intrepbuf);
   if (!strncmp(intrepbuf, "spd", 3))
      spd();
   if (!strncmp(intrepbuf, "getspd", 6))
       printf("\rspd = %d\n",(F1/tmr2)*F2);
}

void __attribute__ ((interrupt, auto_psv)) _U2RXInterrupt(void)
{

   IFS1bits.U2RXIF = 0;
   while (U2STAbits.URXDA) {
      RXbuffer[str_pos] = U2RXREG;
      str_pos++;                //increments the position in the buffer to store the next
      if (str_pos >= RXBSIZE) {
         str_pos = 0;
      }
   }
   if (RXbuffer[str_pos - 1] == '\r') {
      if (!online) {
         printf("\rOK\n");
         online = 1;
         clean_RxB();
      }
      command_avail = 1;
   }

}

void __attribute__ ((interrupt, auto_psv)) _T2Interrupt(void)
{
   _T2IF = 0;
   switch (state) {
   case 1:
      _LATE3 = 1;
      break;
   case 2:
      _LATE3 = 0;
      break;
   case 3:
      _LATE1 = 1;
      break;
   default:
      _LATE1 = 0;
      break;
   }
   state = (state > 2) ? 0 : state + 1;
}

void __attribute__ ((interrupt, auto_psv)) _T3Interrupt(void)
{
   _T3IF = 0;
   PR2 = tmr2;
   i=1;
}

/* main code */
int main(void)
{
   unsigned int UMODEvalue, U2STAvalue, TCONval;

   //Configure pin 2 and 3 of port D as outputs -> LEDS
   _LATE2 = 1;
   _LATE0 = 1;
   _LATE1 = 0;
   _LATE3 = 0;
   _TRISE0 = 0;
   _TRISE1 = 0;
   _TRISE2 = 0;
   _TRISE3 = 0;

   /* Serial port config */
   //activates the uart in continuos mode (no sleep) and 8bit no parity mode
   UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
   //activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
   U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
   OpenUART2(UMODEvalue, U2STAvalue, 15);       //configures and activates UART2 at 115000 bps
   U2STAbits.URXISEL = 1;
   _U2RXIE = 1;                 //0-Interruption off, 1-Interruption on
   U2MODEbits.LPBACK = 0;       //disables hardware loopback on UART2. Enable only for tests
   U2MODEbits.STSEL = 0;

   tmr2 = 30000;
   /*Configures T2 */
   TCONval = T2_ON & T2_GATE_OFF & T2_PS_1_64 & T2_SOURCE_INT;
   OpenTimer2(TCONval, tmr2);
   _T2IE = 1;                   //enable timer 2 interrupt

   /*Configures T3 */
   TCONval = T3_ON & T3_GATE_OFF & T3_PS_1_64 & T3_SOURCE_INT;
   OpenTimer3(TCONval,20000);
   _T3IE = 0;                   //disable timer 3 interrupt

   __C30_UART = 2;              //define UART2 as predefined for use with stdio library, printf etc


   printf("\n\rSerial port ONLINE \n"); //to check if the serial port is working
   /* Begin main execution cycle */
   ramp(10000);
   printf("\rready\n");
   while (1) {
      if (command_avail)
         intrep_command();
   }                            //end while(1)

}                               //end main
