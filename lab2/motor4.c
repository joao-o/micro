#include <libpic30.h>           //C30 compiler definitions
#include <uart.h>               //UART (serial port) function and utilities library
#include <timer.h>              //timer library
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define FCY ((long) 7372*4)     //instruction frequency in kHz

//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16); //oscilator at 16x PLL
_FWDT(WDT_OFF); //watchdog timer is off

#define RXBSIZE 80
#define F1 (unsigned int) 60625 //constants used to compute the timer value
#define F2 (unsigned int) 57    //from rpm and vice-versa

int str_pos = 0;
char RXbuffer[RXBSIZE]; //buffer used to store characters from serial port
char interpbuf[RXBSIZE];

#define NCOMS  4                //number of commands
const char *comm[NCOMS] = {"accel ", "setpar ", "getpar ", "stop "}; //available commands
const int commsz[NCOMS] = {6, 7, 7, 5}; //size of each command in bytes

struct m_val {
    int rpm; //motor speed in rpm
    int tmr; //value in PR2 to attain required speed
    int m_ct; //motor acceleration constant
    int t_acl; //motor acceleration time step
    int t_ini; //motor initialization time
    int tmr_ini; //motor initialization PR2 value
};
struct m_val m = {0, 0, 2048, 300, 400, 30000}; //initialize with values for our motor

int kill;
int command_avail = 0; //flag to indicate command availability
int ticks; //global variable to provide timing from the timer isr

//function to clear rxbuffer

void clean_RxB(void) {
    while (str_pos) {
        str_pos--;
        RXbuffer[str_pos] = 0;
    }
}

void ramp() {
    ticks = 0;
    while (PR2 < m.tmr && kill == 0) {
        if (ticks >= m.t_acl) { //change speed every 5 timer interrupts
            PR2 -= (PR2 / m.m_ct) * (PR2 / m.m_ct) * (PR2 / m.m_ct);
            ticks = 0;
        }
    }
    PR2 = m.tmr; // set the final speed exactly
}

void accel() {
    if (m.rpm == 0) {
        puts("\r please set the correct rpm value");
        return;
    }
    if (m.t_ini != 0) {
        PR2 = m.tmr_ini;
        _T2IE = 1;
        ticks = 0;
        while (ticks < m.t_ini); //wait for the motor to start spinning before accelerating
    }
    ramp();
    puts("\rdone");
}

void dataupd() {
    int a1, a2, a3, a4, a5; //vars to scan arguments
    /*check if "rpm is present in string if not check for five args"*/
    if (!strncmp("rpm", interpbuf + commsz[1], 3)) {
        a1 = atoi(interpbuf + commsz[1] + 3); //scan rpm value
    } else
        if (sscanf(interpbuf + commsz[1], "%d %d %d %d %d", &a1, &a2, &a3, &a4, &a5) != 5) {
        puts("\r not enough args\n"
                "\rinsert <rpm> <m_ct> <t_acl> <t_ini> <tmr_ini> or rpm <rpm>");
        return;
    } else {
        m.m_ct = a2;
        m.t_acl = a3;
        m.t_ini = a4;
        m.tmr_ini = a5;
    }
    m.rpm = a1;
    m.tmr = (F1 / a1) * F2 + ((F1 % a1) * F2) / F1;
}

//interpret command

void interp_command(void) {
    int i;
    kill = 0;
    command_avail = 0;
    memcpy(interpbuf, RXbuffer, str_pos + 1);
    //copy string to temporary buffer to free up RXbuffer
    clean_RxB();
    interpbuf[str_pos - 1] = 0;
    //put a '0' at end of the string to allow usage of stdlib functions
    printf("\r%s\n", interpbuf);
    for (i = 0; i < NCOMS; i++) { //compare read string with available commands
        if (!strncmp(comm[i], interpbuf, commsz[i]))
            break;
    }
    switch (i) { //execute the command
        case 0:
            accel();
            break;
        case 1:
            dataupd();
            break;
        case 2:
            printf("\rspd    = %d\n"
                    "\rtmr    = %d\n"
                    "\rm_ct   = %d\n"
                    "\rt_acl  = %d\n"
                    "\rt_ini  = %d\n"
                    "\rtmr_ini= %d\n",
                    m.rpm, m.tmr, m.m_ct, m.t_acl, m.t_ini, m.tmr_ini);
            break;
        case 3:
            _T2IE = 0;
            break;
        default:
        {
            puts("\rcommand not found\n"
                    "\rcommands available:\n");
            for (i = 0; i < NCOMS; i++) {
                putchar('\r');
                puts(comm[i]);
            }
        }
    }
    //print prompt
    printf("\r> ");
}

void __attribute__((interrupt, auto_psv)) _U2RXInterrupt(void) {
    static int online = 0;

    IFS1bits.U2RXIF = 0;
    while (U2STAbits.URXDA) {
        RXbuffer[str_pos] = U2RXREG;
        str_pos++; //increments the position in the buffer to store the next char
        if (str_pos >= RXBSIZE) {
            str_pos = 0;
        }
    }
    if (RXbuffer[str_pos - 1] == '\r') {
        if (!online) {
            puts("\rOK");
            online = 1;
            clean_RxB();
        } else {
            command_avail = 1;
        }
    }
    if (RXbuffer[str_pos - 1] == 3) {
        kill = 1;
        clean_RxB();
    }
}

//timer2 ISR

void __attribute__((interrupt, auto_psv)) _T2Interrupt(void) {
    static int state = 0;

    _T2IF = 0;

    switch (state) {
        case 0:
            _LATE3 = 1;
            break;
        case 31:
            _LATE3 = 0;
            break;
        case 32:
            _LATE1 = 1;
            break;
        case 63:
            _LATE1 = 0;
            break;
    }
    state = (state > 63) ? 0 : state + 1;
    ticks++;
}

int main(void) {
    unsigned int UMODEvalue, U2STAvalue, TCONval;

    //configure motor pins
    //E0:EN1 E1:IN1 E2:EN2 E3:IN2
    _LATE2 = 1;
    _LATE0 = 1;
    _LATE1 = 0;
    _LATE3 = 0;
    _TRISE0 = 0;
    _TRISE1 = 0;
    _TRISE2 = 0;
    _TRISE3 = 0;

    /* Serial port config */
    //activates the uart in continuous mode (no sleep) and 8bit no parity mode
    UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
    //activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
    U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
    OpenUART2(UMODEvalue, U2STAvalue, 15); //configures and activates UART2 at 115000 bps
    U2STAbits.URXISEL = 1;
    _U2RXIE = 1; //0-Interruption off, 1-Interruption on
    U2MODEbits.LPBACK = 0; //disables hardware loopback on UART2. Enable only for tests
    U2MODEbits.STSEL = 0;

    /*Configures Timer2 */
    TCONval = T2_ON & T2_GATE_OFF & T2_PS_1_8 & T2_SOURCE_INT;
    OpenTimer2(TCONval, 30000);

    __C30_UART = 2; //define UART2 as predefined for use with stdio library, printf etc

    puts("\n\rSerial port ONLINE"); //to check if the serial port is working

    /* Begin main execution cycle */
    while (1) {
        if (command_avail)
            interp_command();
    }
}