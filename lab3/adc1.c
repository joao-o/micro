/*
 * File:   newmain.c
 * Author: user
 *
 * Created on September 30, 2014, 9:33 AM
 */
#include <libpic30.h>           //C30 compiler definitions
#include <uart.h>               //UART (serial port) function and utilities library
#include <timer.h>              //timer library
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <outcompare.h>


#define FCY ((long) 7372*4)     //instruction frequency in kHz

//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16); //oscilator at 16x PLL
_FWDT(WDT_OFF); //watchdog timer is off

#define RXBSIZE 80
#define M_CONST 2048
#define F1 (unsigned int) 11875
#define F2 (unsigned int) 291

int str_pos = 0;
char RXbuffer[RXBSIZE]; //buffer used to store characters from serial port
char interpbuf[RXBSIZE];
#define NCOMS  5
const char* comm[NCOMS] = {"spd", "getspd", "pwmfreq", "pwmduty", "sweep"};
const int commsz[NCOMS] = {3, 6, 7, 7, 5};

int online = 0;
int command_avail = 0;
int state;
unsigned int tmr2;
int ticks;

void delay_ms(int ms) {
    int i;
    for (; ms; ms--)
        for (i = FCY / 4; i; i--);
    return ;
}

void clean_RxB(void) {
    while (str_pos) {
        str_pos--;
        RXbuffer[str_pos] = 0;
    }
}

void ramp(int t) {
    ticks = 0;
    int u = (tmr2 < t) ? 1 : 0;
    while ((tmr2 < t && u) || (tmr2 > t && !u)) {
        {
            if (ticks == 5) {
                if (u) {
                    tmr2 += (tmr2 / M_CONST)*(tmr2 / M_CONST)*(tmr2 / M_CONST);
                } else {
                    tmr2 -= (tmr2 / M_CONST)*(tmr2 / M_CONST)*(tmr2 / M_CONST);
                }
                PR2 = tmr2;
                ticks = 0;
                printf("\r%d\n", tmr2);
            }
        }
    }
    PR2 = t;
    tmr2 = t;

}

void spd() {
    int freq;
    freq = atoi(interpbuf + commsz[0]);
    if (freq > 730 || freq < 110) {
        printf("\rerr: too fast!\n");
        return;
    }
    freq = F1 / freq * F2;
    ramp(freq);
    puts("\rdone");
}

void pwmfreq() {
    int freq = atoi(interpbuf + commsz[2]);
    PR3 = freq;
}

void pwmduty() {
    int duty = atoi(interpbuf + commsz[3]);
    duty = (PR3 / 1024) * duty;
    OC3RS = duty;
}

void sweep() {
    int i;
    for (i = 0; i < 1024; i++) {
        OC3RS = i;
        delay_ms(10);
    }
    for (; i; i--) {
        OC3RS = i;
        delay_ms(10);
    }
    puts("\rdone");
}

void intrep_command(void) {
    int i = 0;
    command_avail = 0;
    memcpy(interpbuf, RXbuffer, str_pos + 1);
    interpbuf[str_pos - 1] = 0;
    clean_RxB();
    printf("\r%s\n", interpbuf);
    for (i = 0; i < NCOMS; i++) {
        if (!strncmp(comm[i], interpbuf, commsz[i]))
            break;
    }
    switch (i) {
        case 0:
            spd();
            break;
        case 1:
            printf("\rspd = %d\n", (F1 / tmr2) * F2);
            break;
        case 2:
            pwmfreq();
            break;
        case 3:
            pwmduty();
            break;
        case 4:
            sweep();
            break;
        default:
            puts("\rcommand not found");
    }

}

void __attribute__((interrupt, auto_psv)) _U2RXInterrupt(void) {

    IFS1bits.U2RXIF = 0;
    while (U2STAbits.URXDA) {
        RXbuffer[str_pos] = U2RXREG;
        str_pos++; //increments the position in the buffer to store the next
        if (str_pos >= RXBSIZE) {
            str_pos = 0;
        }
    }
    if (RXbuffer[str_pos - 1] == '\r') {
        if (!online) {
            puts("\rOK");
            online = 1;
            clean_RxB();
        }
        command_avail = 1;
    }
}

void __attribute__((interrupt, auto_psv)) _T2Interrupt(void) {
    _T2IF = 0;
    switch (state) {
        case 0:
            _LATE3 = 1;
            break;
        case 3:
            _LATE3 = 0;
            break;
        case 4:
            _LATE1 = 1;
            break;
        case 7:
            _LATE1 = 0;
            break;
        default:
            break;
    }
    state = (state > 6) ? 0 : state + 1;
    ticks++;
}

/* main code */
int main(void) {
    unsigned int UMODEvalue, U2STAvalue, TCONval, OC3cfg;

    //configure motor pins
    _LATE2 = 1;
    _LATE0 = 1;
    _LATE1 = 0;
    _LATE3 = 0;
    _TRISE0 = 0;
    _TRISE1 = 0;
    _TRISE2 = 0;
    _TRISE3 = 0;

    //LED cfg
    _LATF0 = 0;
    _TRISF0 = 0;

    /* Serial port config */
    //activates the Uart in continuous mode (no sleep) and 8bit no parity mode
    UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
    //activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
    U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
    OpenUART2(UMODEvalue, U2STAvalue, 15); //configures and activates UART2 at 115000 bps
    U2STAbits.URXISEL = 1;
    _U2RXIE = 1; //0-Interruption off, 1-Interruption on
    U2MODEbits.LPBACK = 0; //disables hardware loopback on UART2. Enable only for tests
    U2MODEbits.STSEL = 0;

    tmr2 = 30000;
    /*Configures T2 */
    TCONval = T2_ON & T2_GATE_OFF & T2_PS_1_64 & T2_SOURCE_INT;
    OpenTimer2(TCONval, tmr2);
    _T2IE = 1; //enable timer 2 interrupt

    /*pwm configuration; initialize at 500hz 50% duty */
    TCONval = T3_ON & T3_GATE_OFF & T3_PS_1_8 & T3_SOURCE_INT;
    OpenTimer3(TCONval, 7372);
    OC3cfg = OC_TIMER3_SRC & OC_PWM_FAULT_PIN_DISABLE;
    OpenOC3(OC3cfg, 3686, 3686);

    __C30_UART = 2; //define UART2 as predefined for use with stdio library, printf etc


    puts("\n\rSerial port ONLINE"); //to check if the serial port is working
    /* Begin main execution cycle */
    ramp(10000);
    while (1) {
        if (command_avail)
            intrep_command();
    } //end while(1)

} //end main

