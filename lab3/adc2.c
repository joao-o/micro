#include <libpic30.h>           //C30 compiler definitions
#include <uart.h>               //UART (serial port) function and utilities library
#include <timer.h>              //timer library
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <outcompare.h>
#include <adc10.h>


#define FCY ((long) 7372*4)     //instruction frequency in kHz

//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16); //oscilator at 16x PLL
_FWDT(WDT_OFF); //watchdog timer is off

#define RXBSIZE 80
#define M_CONST 2048
#define F1 (unsigned int) 11875
#define F2 (unsigned int) 291

int str_pos = 0;
char RXbuffer[RXBSIZE]; //buffer used to store characters from serial port
char interpbuf[RXBSIZE];
#define NCOMS  4
const char* comm[NCOMS] = {"pwmfreq", "pwmduty", "sweep","adcread"};
const int commsz[NCOMS] = {7,7,5,7};

const int sweep_val[8] = {0,20,50,100,200,300,500,1024};
//PWM values for the LED

int online = 0; // flaag to signal that first communication has been made
int command_avail = 0; //flag to signal that a command is available for reading

int data[20];   // holds measurements from the ADC
int adci;       // index for data

void delay_ms(int ms) {
    int i;
    for (; ms; ms--)
        for (i = FCY / 4; i; i--);
    return ;
}

void clean_RxB(void) {
    while (str_pos) {
        str_pos--;
        RXbuffer[str_pos] = 0;
    }
}

void pwmfreq(int freq) {
    PR3 = freq;
    printf("\r%d\n",PR3);
}

void pwmduty(int duty) { 
//recive duty-cycle from intrepreter and put the appropate value at OC3RS
    duty = (PR3 / 1024) * duty;
    OC3RS = duty;
}

void adcread() {
    int i;
    ADCON1bits.ASAM = 1; // signal the ADC to start reading
    while(ADCON1bits.ASAM==1); // wait for the ADC to finish measurement

    for (i=1; i<20 ;i++) { //average the mesurements
       data[0] += data[i];
    }
    data[0] /= 20;
    printf("\rread: %d\n",data[0]);
}

void sweep() {
    int i;
    for (i = 0; i < 8; i++) { // sweeps the chosen values of pwm and read them with the ADC
        OC3RS = sweep_val[i];
        printf("\r%d\n",OC3RS);
        delay_ms(1000);
        adcread();
    }

    puts("\rdone");
}

void intrep_command(void) {
    int i = 0;
    command_avail = 0;
    memcpy(interpbuf, RXbuffer, str_pos + 1);
    interpbuf[str_pos - 1] = 0;
    clean_RxB();
    printf("\r%s\n", interpbuf);

    for (i = 0; i < NCOMS; i++) {
        if (!strncmp(comm[i], interpbuf, commsz[i]))
            break;
    }
    switch (i) {
        case 0:
            pwmfreq(atoi(interpbuf + commsz[0]));
            break;
        case 1:
            pwmduty(atoi(interpbuf + commsz[1]));
            break;
        case 2:
            sweep();
            break;
        case 3:
            adcread();
            break;
        default:
            puts("\rcommand not found");
    }

}

void __attribute__((interrupt, auto_psv)) _ADCInterrupt(void) {
    _ADIF = 0;
    data[adci++]= ADCBUF0;
    if(adci==20) ADCON1bits.ASAM = 0;
}

void __attribute__((interrupt, auto_psv)) _U2RXInterrupt(void) {
    IFS1bits.U2RXIF = 0;
    while (U2STAbits.URXDA) {  // reads bytes from the uart into the buffer
        RXbuffer[str_pos++] = U2RXREG;
        if (str_pos >= RXBSIZE) { // RXbuffer is a circular buffer
            str_pos = 0;
        }
    }
    if (RXbuffer[str_pos - 1] == '\r') { // check for a newline in buffer
        if (!online) {
            puts("\rOK");
            online = 1;
            clean_RxB();
        }else {
            command_avail = 1;
        }
    }
}

/* main code */
int main(void) {
    unsigned int UMODEvalue, U2STAvalue, TCONval, OC3cfg;

    //LED cfg
    _LATF0 = 0;
    _TRISF0 = 0;

    /* Serial port config */
    //activates the uart in continuous mode (no sleep) and 8bit no parity mode
    UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
    //activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
    U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;
    OpenUART2(UMODEvalue, U2STAvalue, 15); //configures and activates UART2 at 115000 bps
    U2STAbits.URXISEL = 1;
    _U2RXIE = 1; //0-Interruption off, 1-Interruption on
    U2MODEbits.LPBACK = 0; //disables hardware loopback on UART2. Enable only for tests
    U2MODEbits.STSEL = 0;

    /*pwm configuration; initialize at max speed 50% duty */
    TCONval = T3_ON & T3_GATE_OFF & T3_PS_1_1 & T3_SOURCE_INT;
    OpenTimer3(TCONval, 1024);
    OC3cfg = OC_TIMER3_SRC & OC_PWM_FAULT_PIN_DISABLE;
    OpenOC3(OC3cfg, 512, 512);

    _T3IE =0;

    __C30_UART = 2; //define UART2 as predefined for use with stdio library, printf etc
   //ADC configuration
   OpenADC10( ADC_MODULE_ON & ADC_IDLE_STOP & ADC_FORMAT_INTG & ADC_CLK_AUTO &
                   ADC_AUTO_SAMPLING_ON & ADC_SAMP_OFF ,
               ADC_VREF_AVDD_AVSS & ADC_CONVERT_CH0 & ADC_SAMPLES_PER_INT_1 &
                   ADC_SCAN_OFF & ADC_ALT_BUF_OFF & ADC_ALT_INPUT_OFF,
               ADC_SAMPLE_TIME_1 & ADC_CONV_CLK_8Tcy & ADC_CONV_CLK_SYSTEM,
               ENABLE_ALL_DIG,
               SCAN_NONE);
    _ADIE = 1;
    ADPCFG = 0x0004; // Selects pin A0 as analog input
    ADCON1bits.ASAM = 0; // turns off auto sampling

    puts("\n\rSerial port ONLINE"); //to check if the serial port is working
    /* Begin main execution cycle */

    while (1) {
        if (command_avail) // waits for commands
            intrep_command();
    } //end while(1)

} //end main
