#include <libpic30.h>           //C30 compiler definitions
#include <uart.h>               //UART (serial port) function and utilities library
#include <timer.h>              //timer library
#include <string.h>
#include <stdio.h>


#define FCY ((long) 7372*4)     //instruction frequency in kHz
#define LED2 LATDbits.LATD3
#define LED1 LATDbits.LATD2

//Configuration bits
_FOSC(CSW_FSCM_OFF & XT_PLL16); //oscilator at 16x PLL
_FWDT(WDT_OFF);                 //watchdog timer is off

#define RXBSIZE 80
char RXbuffer[RXBSIZE];         //buffer used to store characters from serial port
char intrepbuf[RXBSIZE];        
//buffer used to compare characters with specific commands (i.e. toggled)

int str_pos = 0;                //position in the RXbuffer
int online = 0;
int command_avail = 0;

/*Ex. 3*/
void toggle_led(int num)
{
   switch (num) {               //toggles led corresponding to entered number
   case 1:
      asm("btg LATF, #0");
      break;
   case 2:
      asm("btg LATF, #1");
      break;
   case 3:
      asm("btg LATC, #14");
      break;
   case 4:
      asm("btg LATC, #13");
      break;
   default:;
   }
}

void clean_RxB(void)
{
   while (str_pos) {
      str_pos--;
      RXbuffer[str_pos] = 0;
   }
}

//interpret command
void interp_command(void)
{
   command_avail = 0;
   memcpy(intrepbuf, RXbuffer, str_pos + 1); 
     //copy string to temporary buffer to free up RXbuffer
   clean_RxB();
   intrepbuf[str_pos - 1] = 0;               
     //put a '0' at end of the string to allow usage of stdlib functions
   printf("\r%s\n", intrepbuf);
   if (!strncmp(intrepbuf, "toggled ", 7))   
      toggle_led(intrepbuf[7] - '0');   
}

/*timer 3 ISR *//*ex 2i */
void __attribute__ ((interrupt, auto_psv)) _T3Interrupt(void)
{
   _T3IF = 0;                   //clear interrupt flag
   printf("\rg2\n");
}

/*UART 2 ISR*/
void __attribute__ ((interrupt, auto_psv)) _U2RXInterrupt(void)
{

   IFS1bits.U2RXIF = 0;
   while (U2STAbits.URXDA) {
      RXbuffer[str_pos] = U2RXREG;
      str_pos++;                
      if (str_pos >= RXBSIZE) {
         str_pos = 0;
      }
   }
   if (RXbuffer[str_pos - 1] == '\r') {
      if (!online) {            //informs user beginning of communication. Ex 2iii 
         printf("\rOK\n");
         online = 1;
         clean_RxB();
      }
      command_avail = 1;
   }

}

/* main code */
int main(void)
{
   unsigned int UMODEvalue, U2STAvalue, T2CONval;       //auxiliary UART config var

   //Configure pin 2 and 3 of port D as outputs -> LEDS
   _TRISF0 = 0;
   _TRISF1 = 0;
   _TRISC13 = 0;
   _TRISC14 = 0;

   //Configures every PORTB pin as digital I/Os 
   ADPCFG = 0xFFFF;

   /* Serial port config */
   UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;    
     //activates the uart in continuos mode (no sleep) and 8bit no parity mode
   U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX;   
     //enables Tx + enable Rx interrupt for every char
   OpenUART2(UMODEvalue, U2STAvalue, 15);  
     //configures and activates UART2 at 115000 bps
   U2STAbits.URXISEL = 1;
   _U2RXIE = 1;                 //0-Interruption off, 1-Interruption on
   U2MODEbits.LPBACK = 0;       
     //disables hardware loopback on UART2. Enable only for tests

   __C30_UART = 2;    //define UART2 as predefined for use with stdio library 

   /* timer 2/3 config with 1s period with 1:1 mode *//*Ex 2i */
   T2CONval = T2_ON & T2_GATE_OFF & T2_PS_1_1 & T2_32BIT_MODE_ON & T2_SOURCE_INT;
   OpenTimer23(T2CONval, FCY * 1000);
   _T3IE = 1;                   //enable timer 3 interrupt


   printf("\n\rSerial port ONLINE \n"); //to check if the serial port is working

   /* Begin main execution cycle */
   while (1) {
      if (command_avail)
         interp_command();
   }              

}                
