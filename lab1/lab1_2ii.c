/* 
 * File:   newmain.c
 * Author: user
 *
 * Created on September 30, 2014, 9:33 AM
 */
#include <libpic30.h>		        //C30 compiler definitions
#include <uart.h>			//UART (serial port) function and utilities library
#include <timer.h>			//timer library
#include <string.h>

#define FCY ((long) 7372*4) 		//instruction frequency in kHz
#define LED2 LATDbits.LATD3
#define LED1 LATDbits.LATD2

//Configuration bits
 _FOSC(CSW_FSCM_OFF & XT_PLL16);  	//oscilator at 16x PLL
 _FWDT(WDT_OFF);  					//watchdog timer is off

#define RXBSIZE 80
char RXbuffer[RXBSIZE];	//buffer used to store characters from serial port
int str_pos = 0; 	    //position in the RXbuffer

void intrep_command(void)
{
    RXbuffer[str_pos-1] = 0;
    if (!strcmp(RXbuffer,"ledtg"))
        asm ("btg LATF, #0");
    while(str_pos){
        str_pos--;
        RXbuffer[str_pos] = 0;
    }
}

/* 1ms delay function */
void delay_ms(unsigned int delay) //delay in miliseconds
{
    unsigned int cycles;	// number of cycles
    for(;delay;delay--)
        for(cycles=FCY/4;cycles;cycles--); //~1ms cycle
}

void __attribute__((interrupt,auto_psv)) _U2RXInterrupt(void)
{
    IFS1bits.U2RXIF = 0;

    while(U2STAbits.URXDA)
    {
        RXbuffer[str_pos] = U2RXREG;
        str_pos++;						//increments the position in the buffer to store the next
        if(str_pos >= RXBSIZE){str_pos = 0;}
    }
    if (RXbuffer[str_pos-1] == '\r')
        intrep_command();
}

/* main code */
int main(void)
{
	unsigned int UMODEvalue, U2STAvalue, T2CONval; //auxiliary UART config var

	//Configure pin 2 and 3 of port D as outputs -> LEDS
	_TRISD2 = 0;
	_TRISD3 = 0;
        _TRISF0 = 0;
        _LATF0  = 0;

	//Configures every PORTB pin as digital I/Os (wont be using analog inputs for now)
	ADPCFG = 0xFFFF;

	/* Serial port config */
	UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT; //activates the uart in continuos mode (no sleep) and 8bit no parity mode
	U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX; //activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
 	OpenUART2 (UMODEvalue, U2STAvalue, 15); //configures and activates UART2 at 115000 bps
	U2STAbits.URXISEL = 1;
	_U2RXIE = 1; 			//0-Interruption off, 1-Interruption on
	U2MODEbits.LPBACK = 0; 		//disables hardware loopback on UART2. Enable only for tests

	__C30_UART = 2; 		//define UART2 as predefined for use with stdio library, printf etc


	printf("\n\rSerial port ONLINE \n"); 	//to check if the serial port is working

	/* Begin main execution cycle */
	while(1)
	{

	}//end while(1)

}//end main


