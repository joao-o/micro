#include <p30F4011.h>		//defines os dspic registers
#include <stdio.h>			//standart IO library C
#include <libpic30.h>		//C30 compiler definitions
#include <uart.h>			//UART (serial port) function and utilities library
#include <timer.h>			//timer library

#define FCY ((long) 7372*4) 		//instruction frequency in kHz
#define LED2 LATDbits.LATD3
#define LED1 LATDbits.LATD2

//Configuration bits
 _FOSC(CSW_FSCM_OFF & XT_PLL16);  	//oscilator at 16x PLL
 _FWDT(WDT_OFF);  					//watchdog timer is off

char RXbuffer[80];	//buffer used to store characters from serial port
int str_pos = 0; 	//position in the RXbuffer

/* declaration of the delay_ms function*/
void delay_ms(unsigned int delay); //delay in miliseconds

/* main code */
int main(void)
{
	unsigned int UMODEvalue, U2STAvalue, T2CONval; //auxiliary UART config var

	//Configure pin 2 and 3 of port D as outputs -> LEDS
	TRISDbits.TRISD2 = 0;
	TRISDbits.TRISD3 = 0;

	//Configures every PORTB pin as digital I/Os (wont be using analog inputs for now)
	ADPCFG = 0xFFFF;

	/* Serial port config */
	UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT; //activates the uart in continuos mode (no sleep) and 8bit no parity mode
	U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX; //activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
 	OpenUART2 (UMODEvalue, U2STAvalue, 15); //configures and activates UART2 at 115000 bps
	U2STAbits.URXISEL = 1;
	_U2RXIE = 0; 							//0-Interruption off, 1-Interruption on
	U2MODEbits.LPBACK = 0; 					//disables hardware loopback on UART2. Enable only for tests

	__C30_UART = 2; 						//define UART2 as predefined for use with stdio library, printf etc

        /* timer 2/3 config with 1s period with 1:1 mode */
        T2CONval = T2_ON & T2_GATE_OFF & T2_PS_1_1 & T2_32BIT_MODE_ON & T2_SOURCE_INT;
        OpenTimer23(T2CONval,FCY*1000);
        _T3IE = 1; //enable timer 3 interrupt

	printf("\n\rSerial port ONLINE \n"); 	//to check if the serial port is working

	/* Begin main execution cycle */
	while(1) // all work is done in the timer3 ISR
	{

	}

}//end main

/*timer 3 ISR*/
void __attribute__((interrupt,auto_psv)) _T3Interrupt(void)
{
   _T3IF = 0;  //clear interrupt flag
   printf("\rg2\n");
}
