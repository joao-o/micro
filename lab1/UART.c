#include <p30F4011.h>                   //defines os dspic registers
#include <stdio.h>                      //standart IO library C
#include <libpic30.h>                   //C30 compiler definitions
#include <uart.h>                       //UART (serial port) function and utilities library
#include <timer.h>                      //timer library

#define FCY ((long) 7372*4)             //instruction frequency in kHz
#define LED2 LATDbits.LATD3
#define LED1 LATDbits.LATD2

//Configuration bits
 _FOSC(CSW_FSCM_OFF & XT_PLL16);        //oscilator at 16x PLL 
 _FWDT(WDT_OFF);                        //watchdog timer is off

char RXbuffer[80];      //buffer used to store characters from serial port
int str_pos = 0;        //position in the RXbuffer

/* declaration of the delay_ms function*/
void delay_ms(unsigned int delay); //delay in miliseconds

/* main code */
int main(void)
{
        unsigned int UMODEvalue, U2STAvalue; //auxiliary UART config variables 
        
        /*user variables*/
        int number = 0, items = 0, i = 0;
        char letter;    
        char string[30];

        //Configure pin 2 and 3 of port D as outputs -> LEDS
        TRISDbits.TRISD2 = 0;
        TRISDbits.TRISD3 = 0;

        //Configures every PORTB pin as digital I/Os (wont be using analog inputs for now)      
        ADPCFG = 0xFFFF;

        /* Serial port config */
        UMODEvalue = UART_EN & UART_IDLE_CON & UART_NO_PAR_8BIT;
          //activates the uart in continuos mode (no sleep) and 8bit no parity mode
        U2STAvalue = UART_INT_TX & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_RX_TX; 
          //activates interrupt of pin Tx + enables Tx + enable Rx interrupt for every char
        OpenUART2 (UMODEvalue, U2STAvalue, 15); //configures and activates UART2 at 115000 bps
        U2STAbits.URXISEL = 1;
        _U2RXIE = 1;             //0-Interruption off, 1-Interruption on
        U2MODEbits.LPBACK = 0;   //disables hardware loopback on UART2. Enable only for tests

        __C30_UART = 2;          //define UART2 as predefined for use with stdio library, printf etc

        printf("\n\rSerial port ONLINE \n");    //to check if the serial port is working

        /* Begin main execution cycle */
        while(1)
        {
                /* Scanf printf example */

                printf("\rEnter your favorite number, favorite letter, name:\n");       
                  //display user instructions in terminal
                delay_ms(15000);        //waits 15 seconds for user input

                items = sscanf(RXbuffer ,"%d %c %s", &number, &letter, &string);  
                  //analises the string received and stores the values to the given variables 
                printf("\n\rRXbuffer print:\t %s\n", RXbuffer);        
                  //prints RXbuffer content to the console 
                /* prints the values taken from RXbuffer */
                printf("\rNumber of items scanned = %d\n", items);      
                printf("\rFavorite number = %d \n", number);            //if the last position is reached then return to initial positioncrem 
                printf("\rFavorite letter = %c\n", letter);
                printf("\rName = %s \n", string);

                str_pos = 0;    //returns the pointer to position zero in the circular buffer
                for(i=0;i<80;i++){RXbuffer[i] = '\0';}          //erases the buffer

                //end scanf printf test

        }//end while(1)

}//end main

/****************************************************
*                                                   *
*                 Other functions                   *
*                                                   *
****************************************************/
                
/* This is UART2 receive ISR */
void __attribute__((__interrupt__,auto_psv)) _U2RXInterrupt(void)
{
        IFS1bits.U2RXIF = 0;    //resets and reenables the Rx2 interrupt flag
        
        // Read the receive buffer until at least one or more character can be read
    while(U2STAbits.URXDA)
    {
                RXbuffer[str_pos] = U2RXREG;    //stores the last received char in the buffer
                str_pos++;                      //increments the position in the buffer to store the next char
                if(str_pos >= 80){str_pos = 0;}
                  //if the last position is reached then return to initial position 
    }
}

/* 1ms delay function */
void delay_ms(unsigned int delay) //delay in miliseconds
{   
        unsigned int cycles;    // number of cycles
        for(;delay;delay--)             
    for(cycles=FCY/4;cycles;cycles--); //~1ms cycle
}
